# basic-dockerized-node

Basic docker setup I use for my javascript classwork.

Alter the package.json with any dependencies you want, they will be installed on build.

Build the container with a `docker-compose build`, then I recommend a `docker-compose up -d` since there aren't really any useful console logs.

A `docker exec -it js_app /bin/bash` will let you enter the container, dropping you off in /usr/src/app where you will find any files you put into the app folder within this repo.

Simple run those files with `node filename`.

Future module installs can be handled with normal npm commands. All modules are only stored within the container. A `docker-compose down -v` should wipe them out, but leave the package.json intact.

A basic server will be running on port 8080. This serves to keep the container up as well as to become a future jumping off point.